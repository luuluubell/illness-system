﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; 

public class FluScript : MonoBehaviour
{
    public int numberSick; 
    public bool everyoneHealthy = true; 
    public GameObject[] citizens; 
    GameObject sickCitizen; 
    public Text counter; 

    void Start() 
    {
        
    }

    void Update()
    {
        if (everyoneHealthy)
        {
            GetSick(); 
        } else
        {
            NoSick(); 
        }

        if (numberSick == 0)
        {
            everyoneHealthy = true; 
        }
    }

    void GetSick() 
    {
        StartCoroutine("Illness");
    }

    void NoSick ()
    {
        StopCoroutine("Illness"); 
    }

    IEnumerator Illness ()
    {
        yield return new WaitForSeconds(15); //change number to how long you want to wait before the first person is sick


        citizens = GameObject.FindGameObjectsWithTag ("Citizen"); 

        int randomCitizenInt = Random.Range (0, citizens.Length); 
        
        sickCitizen = citizens[randomCitizenInt]; //picks a random sick person in the world

        Debug.Log ("Sick: " + sickCitizen.name);

        sickCitizen.GetComponent<Sick>().enabled = true; //enables the script that runs everythign that has to do with the person being sick and spreading the sickness

        everyoneHealthy = false; 

        MoreSick(); 

    }

    public void MoreSick () //everytime someone is sick this increases the counter
    {
        numberSick += 1; 
        counter.text = "Sick: " + numberSick; 
    }

    public void LessSick () //everytime someone becoems healthy this decreases the counter
    {
        numberSick -= 1;
        counter.text = "Sick: " + numberSick; 
    }

}
